helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab gitlab/gitlab -n gitlab \
  --timeout 600s \
  --set global.hosts.domain=tanzu.lab \
  --set certmanager-issuer.email=adess@tanzu.lab \
  --set global.edition=ce

