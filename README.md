# tkgs

This repository summarizes templates for vSphere with Tanzu Clusters, Clusterrolebindings to leverage the vSphere SSO to grant access to created clusters and simple Demo Applications as well as some handy scripts/logon helpers. 

Use at your own risk. Folder Structure should be self-explanatory.

## Getting started

### To logon you can simply export the neccessary variables:
export KUBECTL_VSPHERE_PASSWORD=XXXXXXXXX
export WCP=WCP-Endpoint-DNS-OR-IP
export USER=administrator@vsphere.local/Your User
export NS=dev (the vSphere-Namespace)
export CLUSTER=tkc-dev (the Clustername in case it´s already created)

### an example logon command
kubectl vsphere login --server $WCP --vsphere-username $USER --tanzu-kubernetes-cluster-namespace $NS --tanzu-kubernetes-cluster-name $CLUSTER --insecure-skip-tls-verify

## Creating a Cluster
You can use the templates in the folder "Cluster-templates" to create your own cluster

Make sure you apply a PSP in the folder "psp" after creation. Otherwise you won´t e able to run any workload

## Demo App

The "Yelb-App" listed here is hosted on a VMW Repository and completly self-contained. It will work on any TKG Cluster which is deployed and has the neccessary acces to the VMW Registry "projects.registry.vmware.com"

