#/bin/bash

k create ns argocd 

kubectl create secret docker-registry regcred --docker-server=docker.io --docker-username=adess --docker-password='XXXXXXXX' --docker-email=adess@vmware.com -n argocd

kubectl patch serviceaccount default \
  -p "{\"imagePullSecrets\": [{\"regcred\": \"image-pull-secret\"}]}" \
  -n argocd

kubectl create secret docker-registry tutorial-registry-credentials \
    --docker-username=adess \
    --docker-password='Gwendy36!$' \
    --docker-server=https://index.docker.io/v1/\
    --namespace default